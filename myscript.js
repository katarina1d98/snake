snakeC = [
    {x: 150, y: 150},
    {x: 140, y: 150},
    {x: 130, y: 150},
    {x: 120, y: 150},
    {x: 110, y: 150}
]

function snakeGame() {
	c = document.getElementById("canvas");
	ctx = c.getContext("2d");
	ctx.canvas.width  = window.innerWidth/2;
  	ctx.canvas.height = window.innerHeight/2;
	width = c.width;
	height = c.height;
	dx=10;
 	dy=0;
	score = 0;
	randomFood();
	main();
		
	
	document.addEventListener("keydown", changeDirection)

	//food[] = randomFood();

}
function main(){
	if(hitWall()){
		clear();
		ctx.textAlign="center"; 
		ctx.font="50px Arial";
		ctx.fillStyle="red";
		ctx.fillText("Game over",width/2,100);
		ctx.font="30px Arial";
		ctx.fillStyle="black";
		ctx.fillText("Your score is "+score,width/2,200);
		return false;
	}

	setTimeout(function onTick() {
		clear();
		ctx.font="15px Arial";
		ctx.fillStyle="black";
		ctx.fillText("SCORE: "+score,10,20);
		drawFood(bonus);
		moveSnake();
		snake();

		main();
	},100)

}

function clear(){
	ctx.fillStyle="white";
	ctx.fillRect(0, 0, c.width, c.height);
}

function moveSnake(){
	const head = {x:snakeC[0].x+dx, y:snakeC[0].y+dy};
	snakeC.unshift(head);

	const eatFood = snakeC[0].x === foodX && snakeC[0].y === foodY;

	const bEatFood = snakeC[0].x === bFoodX && snakeC[0].y === bFoodY;



	if(eatFood){
		score+=1;
		randomFood();
	}else{
		snakeC.pop();
	}
	
	if(bEatFood){
		score+=5;
		randomFood();
	}

}

function changeDirection(event){
	const LEFT_KEY = 37;
 	const RIGHT_KEY = 39;
  	const UP_KEY = 38;
  	const DOWN_KEY = 40;
  	const keyPressed = event.keyCode;
  	const goingUp = dy === -10;
  	const goingDown = dy === 10;
  	const goingRight = dx === 10;
  	const goingLeft = dx === -10;
 	if (keyPressed === LEFT_KEY && !goingRight) {
 	  dx = -10;
 	  dy = 0;
 	}
  	if (keyPressed === UP_KEY && !goingDown) {
  	  dx = 0;
  	  dy = -10;
  	}
  	if (keyPressed === RIGHT_KEY && !goingLeft) {
  	  dx = 10;
  	  dy = 0;
  	}
  	if (keyPressed === DOWN_KEY && !goingDown) {
  	  dx = 0;
  	  dy = 10;
  	}
}

function randomFood(){
	bonus=false;
	bFoodX = -1000;
	bFoodY = -1000;
	foodX=Math.round(Math.random()*(width-10)/10)*10;
	foodY=Math.round(Math.random()*(height-10)/10)*10;
	
	if(Math.random() < 0.7){
		bFoodX=Math.round(Math.random()*(width-10)/10)*10;
		bFoodY=Math.round(Math.random()*(height-10)/10)*10;
		bonus=true;
	}
	drawFood(bonus);
}

function drawFood(bonus){
	ctx.fillStyle = "red";
	ctx.fillRect(foodX,foodY,10,10);
	if(bonus){
		ctx.fillStyle = "green";
		ctx.fillRect(bFoodX,bFoodY,10,10);
	}
	
}

function snake(){
	snakeC.forEach(drawSnakePart);
}

function drawSnakePart(snakePart) {
    ctx.fillStyle = "black";
    ctx.strokeStyle = "white";
    ctx.fillRect(snakePart.x, snakePart.y, 10, 10);
    ctx.strokeRect(snakePart.x, snakePart.y, 10, 10);
}

function hitWall(){
	const hitLeftWall = snakeC[0].x < 0;
  	const hitRightWall = snakeC[0].x > c.width - 10;
  	const hitToptWall = snakeC[0].y < 0;
  	const hitBottomWall = snakeC[0].y > c.height - 10;
  	return hitLeftWall || hitRightWall || hitToptWall ||hitBottomWall;
}